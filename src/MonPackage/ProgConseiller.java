package MonPackage;

import java.util.Scanner;

public class ProgConseiller {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        char choix;
        boolean quitter = false;

        PortefeuilleClients monPortefeuille = new PortefeuilleClients();

        do {
            System.out.println("\nOptions :");
            System.out.println("A. Afficher les comptes");
            System.out.println("B. Tester si la liste est vide");
            System.out.println("C. Rechercher et afficher le solde d'un compte");
            System.out.println("D. Afficher tous les comptes dont le solde est négatif");
            System.out.println("E. Ajouter un nouveau compte");
            System.out.println("F. Modifier le découvert autorisé d'un compte");
            System.out.println("G. Supprimer un compte");
            System.out.println("H. Vider la collection");
            System.out.println("I. Transférer entre un compte Epargne et un compte Courant");
            System.out.println("K. Ajouter un nouveau compte");
            System.out.println("L. Travailler sur l'ensemble des comptes ou un compte spécifique");
            System.out.println("M. Quitter");
            System.out.print("Choisissez votre option : ");

            choix = scanner.next().toUpperCase().charAt(0);

            switch (choix) {
                case 'A':
                    monPortefeuille.afficherComptes();
                    break;
                case 'B':
                    monPortefeuille.testerListeVide();
                    break;
                case 'C':
                    int numeroCompteConsultation = Saisie.lire_int("Quelle est l'id du compte que vous voulez consulter ?");
                    monPortefeuille.rechercherSolde(numeroCompteConsultation);
                    break;
                case 'D':
                    monPortefeuille.afficherComptesNegatifs();
                    break;
                case 'E':
                    int nombreComptes = Saisie.lire_int("Combien de comptes voulez-vous ajouter ?");
                    monPortefeuille.ajouterComptes(nombreComptes);
                    break;
                case 'F':
                    int numeroCompteModification = Saisie.lire_int("Quelle est l'id du compte que vous voulez modifier ?");
                    int nouveauDecouvert = Saisie.lire_int("Quel est le nouveau découvert autorisé ?");
                    monPortefeuille.modifierDecouvert(numeroCompteModification, nouveauDecouvert);
                    break;
                case 'G':
                    int numeroCompteSuppression = Saisie.lire_int("Quelle est l'id du compte que vous voulez supprimer ?");
                    monPortefeuille.supprimerCompte(numeroCompteSuppression);
                    break;
                case 'H':
                    monPortefeuille.viderCollection();
                    break;
                case 'I':
                    int numCompteEpargne = Saisie.lire_int("Entrez le numéro du compte Epargne : ");
                    int numCompteCourant = Saisie.lire_int("Entrez le numéro du compte Courant : ");
                    int montantTransfert = Saisie.lire_int("Entrez le montant du transfert : ");
                    monPortefeuille.transfert(numCompteEpargne, numCompteCourant, montantTransfert);
                    break;
                case 'K':
                    monPortefeuille.ajouterCompte();
                    break;
                case 'L':
                    char choixTravail = monPortefeuille.afficherMenuTravail();
                    switch (choixTravail) {
                        case 'M':
                            monPortefeuille.afficherComptesNegatifs();
                            break;
                        case 'N':
                            double pourcentageAugmentation = Saisie.lire_double("Quel pourcentage d'augmentation ? ");
                            monPortefeuille.augmenterTauxEpargne(pourcentageAugmentation);
                            break;
                        case 'O':
                            monPortefeuille.afficherClientsCourantEpargne();
                            break;
                        default:
                            System.out.println("Option non valide. Veuillez réessayer.");
                    }
                    break;
                case 'M':
                    quitter = true;
                    System.out.println("Au revoir !");
                    break;
                default:
                    System.out.println("Option non valide. Veuillez réessayer.");
            }
        } while (!quitter);
    }
}
