package MonPackage;
class Compte {
    private String nom;
    private int num;
    private int solde;
    private int decouvert;

    public Compte() {
        nom = Saisie.lire_String("Quel est votre nom ? ");
        num = Saisie.lire_int("Quel est votre numéro de compte ? ");
        solde = Saisie.lire_int("Quel est le solde de votre compte ? ");
        decouvert = Saisie.lire_int("Quel est le découvert autorisé de votre compte ? ");
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public int getSolde() {
        return solde;
    }

    public void setSolde(int solde) {
        this.solde = solde;
    }

    public int getDecouvert() {
        return decouvert;
    }

    public void setDecouvert(int decouvert) {
        this.decouvert = decouvert;
    }

    public void consulterSolde() {
        System.out.println("Le solde de votre compte est de : " + solde);
    }

    public void deposer(int montant) {
        solde += montant;
        System.out.println("Dépôt de " + montant + " effectué. Nouveau solde : " + solde);
    }

    public void retirer(int montant) {
        if (montant <= solde + decouvert) {
            solde -= montant;
            System.out.println("Retrait de " + montant + " effectué. Nouveau solde : " + solde);
        } else {
            System.out.println("Opération impossible. Solde insuffisant.");
        }
    }

    public void afficherInfos() {
        System.out.println("Nom : " + nom);
        System.out.println("Numéro de compte : " + num);
        System.out.println("Solde : " + solde);
        System.out.println("Découvert autorisé : " + decouvert);
    }
}