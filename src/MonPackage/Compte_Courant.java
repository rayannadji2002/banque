package MonPackage;

public class Compte_Courant extends Compte {
    
    private int Decouvert_Autorise;  // Utilisation de camelCase pour le nom de l'attribut

    public Compte_Courant(String nom, int num, int solde, int decouvert, int decouvertAutoBis) {
        super();  // Appel du constructeur par défaut de la classe parente
        super.setNom(nom);  // Initialisation des attributs hérités
        super.setNum(num);
        super.setSolde(solde);
        super.setDecouvert(decouvert);
        
        this.Decouvert_Autorise = decouvertAutoBis;  // Initialisation de l'attribut de la classe fille
    }
}
