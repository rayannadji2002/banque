package MonPackage;

public class Compte_Epargne extends Compte {
    private double taux;

    public Compte_Epargne(int numeroCompte, double solde, int numeroClient, double taux, String nomDetenteur) {
        super();
        super.setNum(numeroCompte);
        super.setSolde((int) solde); // Convertir le solde en int
        super.setNom(nomDetenteur);

        this.taux = taux;
    }

    public double getTaux() {
        return taux;
    }

    public void augmenterTaux(double pourcentage) {
        taux += (taux * pourcentage) / 100;
        System.out.println("Taux augmenté avec succès. Nouveau taux : " + taux);
    }
}
