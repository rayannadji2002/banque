package MonPackage;

import java.util.ArrayList;
import java.util.Scanner;

public class PortefeuilleClients {
    private ArrayList<Compte> collection;
    private int nbr;

    public PortefeuilleClients() {
        collection = new ArrayList<>();
        nbr = 0;
    }

    public void ajouterComptes(int nombre) {
        for (int i = 0; i < nombre; i++) {
            Compte compte = new Compte();
            collection.add(compte);
            nbr++;
        }
    }

    public void afficherComptes() {
        for (Compte compte : collection) {
            compte.afficherInfos();
            System.out.println("------------------------");
        }
    }

    public void testerListeVide() {
        if (collection.isEmpty()) {
            System.out.println("Votre liste est vide");
        } else {
            System.out.println("Votre liste n'est pas vide");
        }
    }

    public void rechercherSolde(int numeroCompte) {
        for (Compte compte : collection) {
            if (compte.getNum() == numeroCompte) {
                compte.consulterSolde();
                return;
            }
        }
        System.out.println("Aucun compte trouvé avec le numéro spécifié.");
    }

    public void afficherComptesNegatifs() {
        for (Compte compte : collection) {
            if (compte.getSolde() < 0) {
                compte.afficherInfos();
                System.out.println("------------------------");
            }
        }
    }

    public void ajouterCompte() {
        Compte nouveauCompte = new Compte();
        collection.add(nouveauCompte);
        nbr++;
    }

    public void modifierDecouvert(int numeroCompte, int nouveauDecouvert) {
        for (Compte compte : collection) {
            if (compte.getNum() == numeroCompte) {
                compte.setDecouvert(nouveauDecouvert);
                System.out.println("Découvert modifié avec succès.");
                return;
            }
        }
        System.out.println("Aucun compte trouvé avec le numéro spécifié.");
    }

    public void supprimerCompte(int numeroCompte) {
        for (int i = 0; i < collection.size(); i++) {
            if (collection.get(i).getNum() == numeroCompte) {
                collection.remove(i);
                System.out.println("Le compte a bien été supprimé.");
                return;
            }
        }
        System.out.println("Aucun compte trouvé avec le numéro spécifié.");
    }

    public void viderCollection() {
        collection.clear();
        nbr = 0;
        System.out.println("La collection a été vidée.");
    }

    public void transfert(int numCompteEpargne, int numCompteCourant, int montant) {
        for (Compte compte : collection) {
            if (compte.getNum() == numCompteEpargne) {
                compte.retirer(montant);
            } else if (compte.getNum() == numCompteCourant) {
                compte.deposer(montant);
            }
        }
    }

    public char afficherMenuTravail() {
        System.out.println("\nOptions supplémentaires :");
        System.out.println("M. Afficher la liste des comptes dont le solde est à découvert");
        System.out.println("N. Augmenter le taux de rémunération de tous les comptes Epargne d'un certain %");
        System.out.println("O. Afficher la liste des clients qui possèdent à la fois un compte Courant et un compte Epargne");
        System.out.print("Choisissez votre option : ");

        Scanner scanner = new Scanner(System.in);
        return scanner.next().toUpperCase().charAt(0);
    }

    public void augmenterTauxEpargne(double pourcentage) {
        for (Compte compte : collection) {
            if (compte instanceof Compte_Epargne) {
                ((Compte_Epargne) compte).augmenterTaux(pourcentage);
            }
        }
    }

    public void afficherClientsCourantEpargne() {
        ArrayList<Integer> numerosCourant = new ArrayList<>();
        
        // Trouver les numéros de compte Courant
        for (Compte compte : collection) {
            if (compte instanceof Compte_Courant) {
                numerosCourant.add(compte.getNum());
            }
        }

        // Afficher les clients possédant à la fois un compte Courant et un compte Epargne
        for (Compte compte : collection) {
            if (compte instanceof Compte_Epargne && numerosCourant.contains(compte.getNum())) {
                compte.afficherInfos();
                System.out.println("------------------------");
            }
        }
    }


    private boolean aEpargneCorrespondante(Compte_Courant compteCourant) {
        for (Compte compte : collection) {
            if (compte instanceof Compte_Epargne && compte.getNum() == compteCourant.getNum()) {
                return true;
            }
        }
        return false;
    }
}
